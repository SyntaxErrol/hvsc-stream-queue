const HVSC_DEFAULT_DIR = "./hvsc";
let HVSC_DIR = HVSC_DEFAULT_DIR;

import * as fs from "fs";

const dir = fs.readdir("./", (err, dir) => {

	if (err) return console.log(err);

	let indexed = Object.values(dir).reduce((prev, curr) => {
		let match = curr.match(/(?<=^indexed\.)\d+(?=$)/);
		if (match) curr = +match[0];
		else curr = 0;
		return Math.max(+prev, curr);
	}, 0);

	if (indexed) console.log(`Looks like HVSC#${indexed} is indexed.`);
	else console.log(`Looks like HVSC is not indexed.`);

	fs.readdir(`${HVSC_DIR}/DOCUMENTS`, (err, dir) => {

		if (err) return console.log(err);
		let update = Object.values(dir).reduce((prev, curr) => {
			let match = curr.match(/(?<=^Update)\d+(?=.hvs$)/);
			if (match) curr = +match[0];
			else curr = 0;
			return Math.max(+prev, curr);
		}, indexed);

	if (update > indexed) {
		console.log(`Update #${update} is available in ${HVSC_DIR}!`);
		updateIndex(update);
	} else {
		console.log(`No update is available.`);
	}

	});
});


const updateIndex = function(version) {

	const categories = [
		"DEMOS",
		"GAMES",
		"MUSICIANS",
	];

	let dump = "";

	const readDir = function(paff) {

		let dir;
		try {
			dir = fs.readdirSync(paff, {withFileTypes:true});
		} catch (err) {
			return console.log(err);
		}

		for (let entry of dir) {
			if (entry.isDirectory()) {
				readDir(`${paff}/${entry.name}`);
			} else if (/\.sid$/i.test(entry.name)) {
				let line = `${paff}/${entry.name}`.replace(`${HVSC_DIR}/`, "");
				dump += `${line}\n`;
			}
		}
	}

	categories.forEach((cat) => readDir(`${HVSC_DIR}/${cat}`));

	fs.writeFile(`indexed.${version}`, dump, () => {
		console.log(`HVSC#${version} indexed!`);
	});

};

