import Index  from "../node_modules/flexsearch/dist/module/index.js";
import Document  from "../node_modules/flexsearch/dist/module/document.js";
import WorkerIndex  from "../node_modules/flexsearch/dist/module/worker/index.js";

const HVSC = {};
HVSC.index = new Index("performance");

fetch("/indexed.76")
	.then(res => res.text())
	.then(text => {
		let fullSuggestions = suggestions.cloneNode(true);
		HVSC.text = text;
		HVSC.text.split("\n").forEach((file, idx) => {
			HVSC.index.add(idx, file);
			let opt = document.createElement("option");
			opt.label = file;
			opt.value = idx;
			fullSuggestions.appendChild(opt);
		});
		suggestions.parentElement.replaceChild(fullSuggestions, suggestions);
	});

search.addEventListener("input", ev => {
	clearTimeout(ev.target.debounce);
	ev.target.value = ev.target.value.replace(/[^\w\/\. '-]/, "");
	ev.target.debounce = setTimeout(() => {
		if (!ev.target.value.includes(ev.target.dataset.value)) {
			let results = HVSC.index.search(ev.target.value, 50);
			promoted.replaceChildren();
			let newPromoted = promoted.cloneNode(false);
			results.map(id => newPromoted.appendChild(suggestions.querySelector(`[value="${id}"]`).cloneNode(false)));
			promoted.parentElement.replaceChild(newPromoted, promoted);
		}
		ev.target.dataset.value = ev.target.value;
		console.log(ev.target.value);
	}, 500);
});

search.focus();
