# hvsc-stream-queue

A service for stream audiences to request tracks from the
[High Voltage SID collection](http://hvsc.c64.org).

# status

Currently mere sketching of reading the HVSC song data for an index and how to
efficiently search it in a browser.

```
docker run -d -p 80:80 \
	-v $PWD/:/usr/share/caddy \
	-v caddy_data:/data \
	caddy
```
